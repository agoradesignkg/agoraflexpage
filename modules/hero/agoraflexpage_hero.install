<?php

/**
 * @file
 * Install, update and uninstall functions for the agoraflexpage_hero module.
 */

use Drupal\node\Entity\NodeType;

/**
 * Implements hook_install().
 */
function agoraflexpage_hero_install() {
  \Drupal::moduleHandler()->loadInclude('agorabase', 'inc', 'agorabase.util');

  /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
  $display_repository = \Drupal::service('entity_display.repository');

  /** @var \Drupal\node\NodeTypeInterface|null $node_type */
  if ($node_type = NodeType::load('flex_page')) {
    $field_name = 'field_hero';

    // Assign widget settings for the default form mode.
    $display_repository->getFormDisplay('node', $node_type->id())
      ->setComponent($field_name, [
        'type' => 'media_library_widget',
        'weight' => 1,
        'settings' => [
          'media_types' => [],
        ],
      ])
      ->save();

    // Assign display settings for the 'default' view mode.
    $display_repository->getViewDisplay('node', $node_type->id())
      ->setComponent($field_name, [
        'label' => 'hidden',
        'type' => 'entity_reference_entity_view',
        'weight' => 0,
        'settings' => [
          'view_mode' => 'hero',
          'link' => FALSE,
        ],
      ])
      ->save();
  }

  if (\Drupal::moduleHandler()->moduleExists('lazy')) {
    // Update media.image.hero display settings to explicitly not use lazy.
    $evd = $display_repository->getViewDisplay('media', 'image', 'hero');
    $image_component = $evd->getComponent('field_media_image');
    $image_component['third_party_settings']['lazy'] = [
      'lazy_image' => '0',
      'placeholder_style' => '',
      'data_uri' => FALSE,
    ];
    $evd->setComponent('field_media_image', $image_component);
    $evd->save();
  }

  $styles = ['hero', 'hero_small', 'hero_xl'];
  if (\Drupal::moduleHandler()->moduleExists('focal_point')) {
    foreach ($styles as $style_name) {
      agorabase_migrate_image_style_to_focal_point($style_name);
    }
  }

  // Add webp conversion to image styles.
  foreach ($styles as $style_name) {
    agorabase_add_image_style_webp_conversion($style_name);
  }
}
