<?php

/**
 * @file
 * Utility functions of the agoraflexpage_hero module.
 */

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\NodeType;

/**
 * Adds the hero field to a node type.
 *
 * @param string $node_bundle
 *   The name of the node type.
 */
function agoraflexpage_hero_add_hero_field(string $node_bundle) {
  /** @var \Drupal\node\NodeTypeInterface|null $node_type */
  if ($node_type = NodeType::load($node_bundle)) {
    $field_name = 'field_hero';
    $field_storage = FieldStorageConfig::loadByName('node', $field_name);
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $node_type->id(),
      'label' => 'Hero',
      'translatable' => FALSE,
      'settings' => [
        'handler' => 'default:media',
        'handler_settings' => [
          'target_bundles' => [
            'image' => 'image',
            'video' => 'video',
          ],
          'sort' => [
            'field' => '_none',
            'direction' => 'ASC',
          ],
          'auto_create' => FALSE,
          'auto_create_bundle' => 'image',
        ],
      ],
    ]);
    $field->save();

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');

    // Assign widget settings for the default form mode.
    $display_repository->getFormDisplay('node', $node_type->id())
      ->setComponent($field_name, [
        'type' => 'media_library_widget',
        'weight' => 1,
        'settings' => [
          'media_types' => [],
        ],
      ])
      ->save();

    // Assign display settings for the 'default' view mode.
    $display_repository->getViewDisplay('node', $node_type->id())
      ->setComponent($field_name, [
        'label' => 'hidden',
        'type' => 'entity_reference_entity_view',
        'weight' => 0,
        'settings' => [
          'view_mode' => 'hero',
          'link' => FALSE,
        ],
      ])
      ->save();
  }
}
